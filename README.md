# Minimal Docker for LineageOS - IaC

An approach for remote resources automation using IaC (Infrastructure as Code). In other words, this is a repo focused on some minimal scripts that configure remote instances on DigitalOcean to build LineageOS image there.

The idea of this tool is to use throwaway cloud instances to build LineageOS images and delete them once the build has finished.

[[_TOC_]]

## How to use

### Pre-requisites

1. Create a new DigitalOcean account; you can use **[my referral link to get $100 USD credits for free](https://m.do.co/c/397805aa0e0e)**
2. Generate your read/write [Personal Access Token](https://cloud.digitalocean.com/account/api/tokens)
3. [Add a public SSH key](https://cloud.digitalocean.com/account/security) into your DigitalOcean account and copy the fingerprint of the SSH key shown on that page

### Requirements

- Terraform installed
- Your private SSH key loaded into your ssh-agent
- Clone or download this repo

### Usage

1. (Optional) Check the values of the *Customizable Variables* section into the `./run.sh` script and modify them if you need
2. Make the script executable: `chmod +x ./run.sh`
3. Run the script: `./run.sh <create|destroy> <YOUR_DO_TOKEN> <YOUR_SSH_KEY_FINGERPRINT>`

### Examples

>**Note:** the values shown in this example where randomly generated at the moment of writing this guide. Don't try to use them on the script.

You have the following values for your token and SSH key fingerprint:

`DigitalOcean personal access token: 23dd3d82cc3361b699a32ec9025868a35df6fb40c5847c89c0cc258eeb9df774`

`Public SSH key fingerprint: 08:97:07:95:8e:7a:59:8f:a4:65:56:8b:dd:a7:93:30`

If you want to create your resources, execute `./run.sh create "23dd3d82cc3361b699a32ec9025868a35df6fb40c5847c89c0cc258eeb9df774" "08:97:07:95:8e:7a:59:8f:a4:65:56:8b:dd:a7:93:30"`

If you want to delete your previously created resources, execute `./run.sh destroy "23dd3d82cc3361b699a32ec9025868a35df6fb40c5847c89c0cc258eeb9df774" "08:97:07:95:8e:7a:59:8f:a4:65:56:8b:dd:a7:93:30"`

## FAQ

### Do I need this to build LineageOS?

No, this is an optional tool, but useful if you prefer run the build system on DigitalOcean.

### What are the reasons of this repo?

If you have a shitty Internet service, a potato computer or not enough disk space to download all build dependencies, you should consider create your own LOS builds on the cloud.

### Are the cloud resources destroyed once the build is finished?

**NO**, you need to backup somewhere else your new built images if you want delete the cloud resources. If you don't destroy the resources, you will be paying for them and your free referral credits might be wasted even faster than COVID-19 spread on your country.

My suggestion is only keep the disk volume created with this tool and destroy the droplet. Next time, the build should be much faster because most of the BIG repos are already downloaded and also the build process will be faster if you leave `ccache` enabled; this way you only need to pay for the volume disk space allocated.

If you prefer the modest approach, you can delete all the resources and next time the build will start from scratch, but that approach will take much longer, as you're executing everything from scratch.

### Why didn't you choose a container distribution to create the droplet?

I did it. In fact I loved the idea to simply run our custom Docker image on a droplet based on RancherOS or CoreOS, but unfortunately [you cannot auto-attach (format+mounting) the volume on these distributions](https://www.digitalocean.com/docs/volumes/#limits).

I tried to did that on the script, but there is no simple way to achieve this. The idea here is let DigitalOcean attach the volume and we just take care of installing Docker and run our Docker image to build LineageOS.

Also, the container distribution does not have integrated DO's monitoring features.

### How can I view the list of available distros to configure on the script?

You should do some DO's API requests; the token is the same you already generated to use in this script. Please check [official API docs](https://developers.digitalocean.com/documentation/v2/#list-all-images) and take a look into the `slug` names, those are the values you can use on the `DO_IMAGE` variable.

## Credits and license

This is an open-source project by **@Fremontt** under MIT License.
