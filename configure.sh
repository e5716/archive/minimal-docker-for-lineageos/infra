#!/bin/bash

# ==================================================== #
#                   Variables section
# ==================================================== #


# --------------> CUSTOMIZABLE VARIABLES <-------------

# (Optional) You can modify the values listed here
DO_REGION="sfo2"
DO_DROPLET_NAME="LineageOS-MicroG-Docker"
DO_DROPLET_SIZE="c-4" # I use the "cpu-optimized plan - $80 USD/month" droplet type; taken from https://developers.digitalocean.com/documentation/v2/#list-all-sizes
DO_VOLUME_NAME="losmicrogbuild" # Name shown on DigitalOcean web interface (only alphanumeric and lowercase, don't use other symbols)
DO_VOLUME_SIZE="250" # You should reduce this value if you have a limited budget, but consider build LineageOS takes a lot of disk space; expressed in GB

# ---------------> ADVANCED OPTIONS <------------------

# I don't recommend changing these variables, unless you know what are you doing
# Note: RancherOS or CoreOS on DigitalOcean only support login via SSH keys
DO_VOLUME_FSLABEL="los_build_files" # TODO: evaluate use this to automount on RancherOS/CoreOS
DO_IMAGE="centos-7-x64"
SSH_LOGIN_USER="root"
TF_EXECPLAN_FOLDER="tf-execution-plan"
DOCKERIMG_REPO="https://gitlab.com/fremontt/minimal-docker-for-lineageos/image.git"
DOCKERIMG_TAG="fremontt/losmicrog:latest"
LOCAL_BASE_DIR="minimal-los-microg"
LOCAL_SWAP_SIZE="16384" # MB
# Note for me: if you want to use CoreOS/RancherOS one day, don't forget the images are called "rancheros"/"coreos-stable" and you must login via SSH with "rancher"/"core" users

# ==================================================== #
#       Logic section: don't touch anything else
# ==================================================== #
ACTION="${1}"
DO_TOKEN="${2}"
DO_SSH_FINGERPRINT="${3}"

function printMessage
{
    NOW=$(date +"%Y-%m-%d %T")
    echo "[$NOW] ${1}"
}
function finishError
{
    printMessage "Error: ${1}"
    exit 1
}
function printInfo
{
    printMessage "Execution details for the cloud provisioning"
    echo -e "\tAction: ${ACTION}"
    echo -e "\tDO personal access token: ${DO_TOKEN}"
    echo -e "\tDO SSH key fingerprint: ${DO_SSH_FINGERPRINT}"
}
function checkRequiredVariables
{
    if [ -z "$DO_TOKEN" ];
    then
        finishError "No DigitalOcean account token is set for this script"
    fi
    if [ -z "$DO_SSH_FINGERPRINT" ];
    then
        finishError "No SSH key fingerprint is set for this script"
    fi
}
function tfActionsCreate
{
    printMessage "Creating resources on your DigitalOcean account"
    terraform init || finishError "Could not start Terraform"
    terraform plan -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="diskName=${DO_VOLUME_NAME}" -var="diskSize=${DO_VOLUME_SIZE}" -var="image=${DO_IMAGE}" -var="fsLabel=${DO_VOLUME_FSLABEL}" -out "${TF_EXECPLAN_FOLDER}" || finishError "Could not create a Terraform plan"
    terraform apply "${TF_EXECPLAN_FOLDER}" || finishError "Could not apply changes into your DigitalOcean account"
}
function nextStepsSSH
{
    printMessage "Running remote commands to configure the build env"
    # This is called HereDoc and must be at the beginning of the line
    ssh -o StrictHostKeyChecking=no "${SSH_LOGIN_USER}@$(terraform output droplet_ip)" /bin/bash << REMOTECOMMANDS
        # Create base dir for build
        mkdir "/mnt/${DO_VOLUME_NAME}/${LOCAL_BASE_DIR}/" || exit 1
        # Create swapfile: be careful about the size as you can run out of space on the system disk
        dd if=/dev/zero of=/swap count=${LOCAL_SWAP_SIZE} bs=1M status=progress || exit 1 # TODO: evaluate using fallocate
        chmod 0600 /swap || exit 1
        mkswap /swap || exit 1
        swapon /swap || exit 1
        echo '/swap none swap defaults 0 0' >> /etc/fstab || exit 1
        # Install Docker
        yum -y update || exit 1
        yum -y install git yum-utils epel-release || exit 1
        yum-config-manager --add-repo 'https://download.docker.com/linux/centos/docker-ce.repo' || exit 1
        yum -y install docker-ce docker-ce-cli containerd.io || exit 1
        # Install a recent version of Git to clone the image repo, as CentOS does not include a 2.x git version
        yum -y install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm || exit 1
        yum -y install git tmux || exit 1
        systemctl enable docker || exit 1
        systemctl start docker || exit 1
        # Only build our image, but do not run yet
        docker build -t ${DOCKERIMG_TAG} ${DOCKERIMG_REPO} || exit 1
        exit
REMOTECOMMANDS
}
function tfActionsDestroy
{
    terraform plan -destroy -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="diskName=${DO_VOLUME_NAME}" -var="diskSize=${DO_VOLUME_SIZE}" -var="image=${DO_IMAGE}" -var="fsLabel=${DO_VOLUME_FSLABEL}" -out "${TF_EXECPLAN_FOLDER}" || finishError "Could not update the Terraform plan with your current resources"
    terraform destroy -auto-approve -var="accountToken=${DO_TOKEN}" -var="sshFingerprint=${DO_SSH_FINGERPRINT}" -var="dropletName=${DO_DROPLET_NAME}" -var="dropletRegion=${DO_REGION}" -var="dropletSize=${DO_DROPLET_SIZE}" -var="diskName=${DO_VOLUME_NAME}" -var="diskSize=${DO_VOLUME_SIZE}" -var="image=${DO_IMAGE}" -var="fsLabel=${DO_VOLUME_FSLABEL}" || finishError "An error has occured when trying to delete your cloud resources"
}
function init
{
    checkRequiredVariables
    printInfo

    case $ACTION in     
        "create")
            tfActionsCreate
            nextStepsSSH
            printMessage "Please do not delete ${TF_EXECPLAN_FOLDER} folder, otherwise you could have issues when deleting the resources at DigitalOcean"
            ;;
        "destroy")
            tfActionsDestroy
            ;;
        *)
            finishError "No action specified. Please run '$0 <action: create|destroy> <do_token> <do_ssh_fingerprint>'"
            ;;
    esac
}

init