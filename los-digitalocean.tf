# Terraform config file for DigitalOcean resources
# Author: @Fremontt

# Variables must be declared here before assigning a value on "terraform.tfvars"
variable "accountToken" {}
variable "sshFingerprint" {}
variable "dropletName" {}
variable "dropletRegion" {}
variable "dropletSize" {}
variable "diskName" {}
variable "diskSize" {}
variable "image" {}
variable "fsLabel" {}

# Set up the provider: you only need an access token generated from your account
provider "digitalocean" {
  token = var.accountToken
}

# Resources for LineageOS build architecture
resource "digitalocean_droplet" "tf_droplet_los" { # The second argument is the name of the resource that will be used by Terraform references (for example, the output)
  name = var.dropletName
  region = var.dropletRegion
  size = var.dropletSize
  image =  var.image # We need an image with Docker-ready support
  backups = false
  monitoring = true
  ipv6 = false
  private_networking = false
  ssh_keys = [var.sshFingerprint]
  resize_disk = false
  tags = ["lineageos", "docker-los"]
}
resource "digitalocean_volume" "tf_volume_los" {
  region = var.dropletRegion # Must be the same region of the droplet
  name = var.diskName
  size = var.diskSize
  description = "Disk volume to save the source repos, build binaries and generated build images of LineageOS"
  initial_filesystem_type = "ext4"
  initial_filesystem_label = var.fsLabel
  tags = ["lineageos", "docker-los"]
}
resource "digitalocean_volume_attachment" "tf_volume_attach_to_droplet" {
  droplet_id = digitalocean_droplet.tf_droplet_los.id
  volume_id = digitalocean_volume.tf_volume_los.id
}

# Output variables
output "droplet_ip" { # From "data" section at https://www.terraform.io/docs/providers/do/d/droplet.html
  value = digitalocean_droplet.tf_droplet_los.ipv4_address
}
output "volume_urn" {
  value = digitalocean_volume.tf_volume_los.urn
}